<?php

namespace Sng\Additionalscheduler;

/*
 * This file is part of the "additional_scheduler" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */

abstract class AdditionalFieldProviderInterface implements \TYPO3\CMS\Scheduler\AdditionalFieldProviderInterface
{

}